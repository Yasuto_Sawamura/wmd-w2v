# -*- coding:utf-8 -*-
import gensim,time
import MeCab,numpy,sys,re

args=sys.argv
#args[1]が入力するモデル、args[2]が出力するマトリックス名(拡張子は自動付与)

class MeC:
	def __init__(self):
		self.t=MeCab.Tagger("-Ochasen")
		self.r=re.compile("記号-")
	
	def wac(self,a):
		a5=[]
		for p in a:
			a4=[]
			p2=p.strip().replace("\r\t","").replace("\r","").replace("\n","").replace("<br>","")
			mres=self.t.parse(p2).split("\n")
			
			for q in mres:
				if q=="EOS" or q=="":
					continue
				k=q.split("\t")
				
				if self.r.match(k[3])==None:
					a4.append(k[2])
			
			a5.append(" ".join(a4))
		
		return a5
	
	def read_file(self,f_name):
		a3=[]
		fp1=open(f_name,"r")
		for p in fp1:
			a1=p.strip().split("\t")
			a3.append("".join(a1[2:]))
		
		a4=self.wac(a3)
		
		fp1.close()
		return a4


f_pass="recipe_500/ckpd_"
tpc=500# texts per category
filelist=["Fried_Chicken500.tsv","Hamburger_Steak500.tsv",
"Miso_Soup500.tsv","Potage500.tsv"]

a=time.time()
print args[1],"→",args[2]

ll=len(filelist)#lengh of list
wmd=gensim.models.word2vec.Word2Vec.load(args[1])
M=MeC()
recipe_sim_list=numpy.zeros([ll*tpc,ll*tpc])

recipe_wac_list=[M.read_file(f_pass+x) for x in filelist]

for p in range(ll):
	for q in range(ll):
		if p>q:
			continue
		for r in range(tpc):
			for s in range(tpc):
				if r>s and p==q:
					continue
				recipe_sim_list[p*tpc+r][q*tpc+s]=wmd.wmdistance(recipe_wac_list[p][r].split(" "),recipe_wac_list[q][s].split(" "))

numpy.save(args[2],recipe_sim_list)
print "processing time",time.time()-a,"(s)"
print "success!"
print 
