# -*- coding:utf-8 -*-
import d2vv4
from sklearn.svm import SVC
from sklearn.grid_search import GridSearchCV
import numpy,random

#f_pass="/home/yasuto/pro2/recipes/ckpd_"
DATA_NUM=500

filelist=["ckpd_Fried_Chicken500.tsv","ckpd_Hamburger_Steak500.tsv","ckpd_Miso_Soup500.tsv","ckpd_Potage500.tsv"]
fnum=len(filelist)

#model_file="/media/yasuto/ボリューム/pro/copus/ckpd_model_400k_1121.d2v"#モデルファイル
model_file="/media/yasuto/ボリューム/ckpd_model/ckpd_modek_2m_1215.d2v"

model=d2vv4.d2vv(model_file,r_mode=0)

recipe_v_list=model.read_files(filelist)

param_dict=[
	{"kernel":["rbf"],"C":[0.01,1.0,10,100,1000,10000,100000],"gamma":[100000,10000,1000,100,10,1,0.1,0.01,0.001]},
	{"kernel":["linear"],"C":[0.01,0.1,1,10,100,1000,10000,100000]},
	{"kernel":["poly"],"C":[0.01,0.1,1,10,100,1000,10000,100000],"gamma":[100000,10000,1000,100,10,1,0.1,0.01,0.001]},
	{"kernel":["sigmoid"],"C":[0.01,0.1,1,10,100,1000,10000,100000],"gamma":[100000,10000,1000,100,10,1,0.1,0.01,0.001]},
]

fp=open("out_svm4-x3-gs-2m.tsv","w")

for p in range(fnum):
	
	tmp_veclist=[]
	for q in range(fnum):
		if q==p:
			continue
		for r in range(DATA_NUM):
			tmp_veclist.append([recipe_v_list[q][r],0])
	
	for q in range(fnum-1):
		for r in range(DATA_NUM):
			tmp_veclist.append([recipe_v_list[p][r],1])
	
	random.shuffle(tmp_veclist)
	
	clf=GridSearchCV(SVC(decision_function_shape="ovr"),param_dict,scoring="f1_micro",n_jobs=-1)
	clf.fit([x[0] for x in tmp_veclist],[y[1] for y in tmp_veclist])
	
	#コンフュージョンマトリックスでチェック
	tmp_veclist2=[]
	for q in range(fnum):
		for r in range(DATA_NUM):
			tmp_veclist2.append(recipe_v_list[q][r])
	mxr=clf.predict(tmp_veclist2)
	
	res_mx=[[0.0,0.0],[0.0,0.0]]
	for s in range(len(mxr)):
		if DATA_NUM*p<=s and s<DATA_NUM*(p+1):
			if mxr[s]==1:
				res_mx[1][1]+=1.0
			else:
				res_mx[1][0]+=1.0
		else:
			if mxr[s]==0:
				res_mx[0][0]+=1.0
			else:
				res_mx[0][1]+=1.0
	#以上チェック部分
	
	fp.write(filelist[p]+"\n")
	fp.write("clf.best_estimator_:"+str(clf.best_estimator_)+"\n")
	fp.write("clf.best_params_"+str(clf.best_params_)+"\n")
	fp.write("clf.best_score_"+str(clf.best_score_)+"\n")
	fp.write(str(res_mx[0][0])+"\t"+str(res_mx[0][1])+"\n")
	fp.write(str(res_mx[1][0])+"\t"+str(res_mx[1][1])+"\n\n")
	fp.flush()

fp.close()
