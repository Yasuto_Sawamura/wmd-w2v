# -*- coding:utf-8 -*-
import gensim
import MeCab
import numpy
import math
import re
#devv.pyから、ベクトルの正規化機能をオプションにしたもの
#d2vv.pyとの互換性あり。(コンストラクタの引数が2つの場合正規化しない)
class d2vv():
	def __init__(self,saved_model,r_mode=1):
		self.t=MeCab.Tagger("-Ochasen")
		self.model=gensim.models.doc2vec.Doc2Vec.load(saved_model)
		self.rr=re.compile("記号")
		#1ならベクトルを正規化する。0なら正規化しない
		self.r_mode=r_mode
	
	def infer(self,input_lines):
		b4=[]
		for p in input_lines:
			if p=="" or p=="\n":
				continue
			b1=self.t.parse(p.strip()).split("\n")
			b3=""
			for q in b1:
				if q=="EOS" or q=="" or q=="EOS\n":
					continue
				b2=q.split("\t")
				
				if self.rr.match(b2[3])==None:
					b3+=b2[2]+" "
				
			self.model.random = numpy.random.RandomState(1) 
			non_r_vector=self.model.infer_vector(b3.split(" "))
			
			r_vector=[]
			vs=0
			if self.r_mode==1:
				for p in non_r_vector:
					vs+=p*p
				vs=math.sqrt(vs)
				for p in non_r_vector:
					r_vector.append(p/vs)
				b4.append(numpy.array(r_vector))
			
			else:
				b4.append(numpy.array(non_r_vector))
		
		return b4
	
	def read_files(self,filelist):
		
		recipe_v_list=[]
		for r in filelist:
			a3=[]
			fp1=open(r,"r")
			for p in fp1:
				a1=p.strip().split("\t")
				a2=""
				for q in a1[2:]:
					a2+=q
			
				a3.append(a2)
			
			a4=self.infer(a3)
			recipe_v_list.append(a4)
			fp1.close()
		
		return recipe_v_list
	
	