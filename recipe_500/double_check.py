# -*- coding:utf-8 -*-
#filelist内のレシピで重複しているものがないかチェック。なければ何も出力しない。ダブっていれば、そのIDを出力
filelist=["Fried_Chicken500.tsv","Hamburger_Steak500.tsv","Miso_Soup500.tsv","Potage500.tsv"]
idl=[]

for p in filelist:
	fp=open("ckpd_"+p,"r")
	for q in fp:
		r=q.split("\t")
		if r[0] in idl:
			print r[0]
		else:
			idl.append(r[0])
	fp.close()
print len(idl)