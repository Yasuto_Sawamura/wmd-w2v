# -*- coding:utf-8 -*-
import d2vv4
from sklearn.svm import SVC
import numpy,random

f_pass="ckpd_"
DATA_NUM=100
save_file="out_svm4x1_step3-2m.tsv"
filelist=["ckpd_Fried_Chicken500.tsv","ckpd_Hamburger_Steak500.tsv","ckpd_Miso_Soup500.tsv","ckpd_Potage500.tsv"]
fnum=len(filelist)
d=5#分割数

model_file="/media/yasuto/ボリューム/ckpd_model/ckpd_modek_2m_1215.d2v"#モデルファイル
#model_file="../../pro/copus/ckpd_20_1110.d2v"#モデルファイル
model=d2vv4.d2vv(model_file,r_mode=0)

#param_list=[["rbf",10,100000],["rbf",0.01,100000],["rbf",10,100000],["rbf",0.01,100000]]

def calf(ac,re):
	if ac+re!=0:
		return (2.0*ac*re)/(ac+re)
	else:
		return 0

fp=open(save_file,"w")
fp.write(str(d)+"分割交差検定の結果")

recipe_v_list=model.read_files(filelist)

result_5d=[[] for q in range(fnum)]

#pの方はラベル1,他はラベル0となる
for p in range(fnum):
	
	#d分割交差検定
	for r in range(d):
		l_datas=[]#学習データ
		test_v0=[]#分類するデータのリスト0
		test_v1=[]#分類するデータのリスト1
		
		for q in range(fnum):
			if q==p:
				continue
			for s in range(DATA_NUM):
				if (DATA_NUM/d)*r<=s and s<(DATA_NUM/d)*(r+1):
					test_v0.append(recipe_v_list[q][s])
				else:
					l_datas.append([recipe_v_list[q][s],0])
		
		
		#for q in range(fnum-1):
		for q in range(1):
			for s in range(DATA_NUM):
				if s<(DATA_NUM/d)*r or (DATA_NUM/d)*(r+1)<=s:
					l_datas.append([recipe_v_list[p][s],1])
		
		for s in range(DATA_NUM):
			if (DATA_NUM/d)*r<=s and s<(DATA_NUM/d)*(r+1):
				test_v1.append(recipe_v_list[p][s])
		
		random.shuffle(l_datas)
		
		#esti=SVC(kernel=param_list[p][0],C=param_list[p][1],gamma=param_list[p][2],decision_function_shape="ovo")
		esti=SVC(decision_function_shape="ovr",class_weight={1:3})
		esti.fit([x[0] for x in l_datas],[y[1] for y in l_datas])
		
		test_v=test_v0+test_v1
		#分類実行
		res_lab=esti.predict(test_v)
		
		#ラベル0の文で0に分類されたもの/1に分類されたもの(p)
		#ラベル1の文で0に分類されたもの/1に分類されたもの(q)
		res_mx=[[0,0],[0,0]]
		for s in range((DATA_NUM/d)*fnum):
			if s<(DATA_NUM/d)*(fnum-1):
				if res_lab[s]==0:
					res_mx[0][0]+=1
				else:
					res_mx[0][1]+=1
			else:
				if res_lab[s]==1:
					res_mx[1][1]+=1
				else:
					res_mx[1][0]+=1
		
		result_5d[p].append(res_mx)

result_ac=[ [] for q in range(fnum)]
result_re=[ [] for q in range(fnum)]

for p in range(fnum):
	
	tmp_ac0=0.0#p
	tmp_ac1=0.0#q
	tmp_re0=0.0#p
	tmp_re1=0.0#q
	
	#d回分の結果の平均を計算
	for r in range(d):
		if sum([x[0] for x in result_5d[p][r]])!=0: 
			tmp_ac0+=1.0*result_5d[p][r][0][0]/sum([x[0] for x in result_5d[p][r]])
		else:
			tmp_ac0+=0
		if sum([x[1] for x in result_5d[p][r]])!=0:
			tmp_ac1+=1.0*result_5d[p][r][1][1]/sum([x[1] for x in result_5d[p][r]])
		else:
			tmp_ac1+=0
		tmp_re0+=1.0*result_5d[p][r][0][0]/sum(result_5d[p][r][0])
		tmp_re1+=1.0*result_5d[p][r][1][1]/sum(result_5d[p][r][1])
	
	result_ac[p].append(tmp_ac0/d)
	result_ac[p].append(tmp_ac1/d)
	result_re[p].append(tmp_re0/d)
	result_re[p].append(tmp_re1/d)

result_f=[]
for p in range(fnum):
	tmp_ar1=[p,]
	tmp_ar1.append(calf(result_ac[p][1],result_re[p][1]))
	result_f.append(tmp_ar1)

result_f_sorted=sorted(result_f,key=lambda x:x[1])

whole_ac=0
whole_re=0

fp.write("\n\nカテゴリごとの結果\n")
fp.write("カテゴリ\t精度\t再現率\tF値\n")
for p in range(fnum):
	writeline=""
	writeline+=filelist[p]+"\t"
	
	whole_ac+=result_ac[p][1]
	whole_re+=result_re[p][1]
	
	wac=result_ac[p][1]
	wre=result_re[p][1]
	
	writeline+=str(wac)+"\t"+str(wre)+"\t"+str(calf(wac,wre))+"\n"
	fp.write(writeline)

whole_ac/=fnum
whole_re/=fnum

fp.write("\n全体の結果\n")
fp.write("精度\t再現率\tF値\n")
writeline=str(whole_ac)+"\t"+str(whole_re)+"\t"+str(calf(whole_ac,whole_re))+"\n"
fp.write(writeline)
