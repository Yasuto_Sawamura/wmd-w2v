# -*- coding:utf-8 -*-
import wmd_exph,sys

rpc=500
result_out="wmd_w2v_aa_fresult2.tsv"
filelist=["Fried_Chicken500.tsv","Hamburger_Steak500.tsv","Miso_Soup500.tsv","Potage500.tsv"]

cat_num=len(filelist)
args=sys.argv
A=wmd_exph.set_result(args[1])

true_d=A.cal_average_byr(True);print len(true_d[0])
false_d=A.cal_average_byr(False);print len(false_d[0])

fp=open(result_out,"a")

allres=[]
for p in range(cat_num):
	t_sum=0;t=0;f_sum=0;f=0;
	for q in range(rpc):
		t_sum+=true_d[p*rpc+q][1];t+=1;
		f_sum+=false_d[p*rpc+q][1];f+=1;
		"""if false_d[p*rpc+q][1]!=0:
			print "a","""
	
	allres.append([(1.0*t_sum)/t,(1.0*f_sum)/f])

fp.write("input_array : "+args[1]+"\n")
for p in range(cat_num):
	wl=filelist[p]+"\naverage distance between same category,"+str(allres[p][0])+"\n"
	wl+="average distance between other category,"+str(allres[p][1])+"\n"
	fp.write(wl)

fp.write("\nall(same category),"+str(sum([x[0] for x in allres])/len(allres)))
fp.write("all(othe category),"+str(sum([x[1] for x in allres])/len(allres)))
