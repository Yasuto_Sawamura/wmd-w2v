# -*- coding:utf-8 -*-

import gensim
import sys

args=sys.argv
#arg1は出力するモデル名

#入力コーパス
copus="ckpd_wakati_10k_0828.wak"

print "input file:",copus,"→ output:",args[3]
print "model dimension:",args[1],"window size:",args[2]
print

W=gensim.models.word2vec.LineSentence(copus)
model=gensim.models.word2vec.Word2Vec(W,sg=1,size=int(args[1]),window=int(args[2]),seed=1,iter=10,workers=6)

model.save(args[3])
