# -*- coding:utf-8 -*-
import numpy

cat_num=4#category number
rpc=500#recipe per category

class set_result:
	def __init__(self,load_file):
		a=numpy.load(load_file)
		self.r=[]
		self.r_sum= [0 for x in range(cat_num*rpc)]
		self.r_ssum=[0 for x in range(cat_num)]
		
		for p in range(cat_num):
			for s in range(rpc):
				tmp_r=[]
				
				for q in range(cat_num):
					for t in range(rpc):
						
						if p>q:
							tmp_r.append([a[q*rpc+t][p*rpc+s],False,q,t])
						elif p<q:
							tmp_r.append([a[p*rpc+s][q*rpc+t],False,q,t])
						elif p==q and s>t:
							tmp_r.append([a[q*rpc+t][p*rpc+s],True,q,t])
						elif p==q and s<t:
							tmp_r.append([a[p*rpc+s][q*rpc+t],True,q,t])
						else:
							pass
				
				self.r.append(sorted(tmp_r,key=lambda x:x[0]))
				#self.r.append(tmp_r)
		"""
		for p in range(cat_num):
			for s in range(rpc):
				for v in range(rpc-1):
					if self.r[p*rpc+s][v][1]==True:
						self.r_sum[p*rpc+s]+=1
				
				self.r_sum[p*rpc+s]/=1.0*(rpc-1)
			
			self.r_ssum[p]=sum(r_sum[rpc*p:rpc*(p+1)])/rpc
		"""
	"""
	def re_allsum(self):
		return self.r_ssum
	
	def re_recipesum(self):
		return self.r_sum
	"""
	def re_xTopSimilar(self,recipe_num,x):
		return self.r[recipe_num][:x]
	
	def cal_average_byr(self,b):
		#b is True/False which selects recipes of same category or others.
		#number of inf, average of distance(disgard inf)
		allres=[]
		
		for p in range(cat_num):
			for s in range(rpc):
				
				res=[0,0.0];not_inf=0;alnum=0;
				for q in self.r[p*rpc+s]:
					if q[1]==b:
						alnum+=1
						if q[0]==float("inf"):
							res[0]+=1
						else:
							not_inf+=1
							res[1]+=q[0]
				
				if not_inf!=0:
					res[1]/=not_inf*1.0
					#print res[1],
				else:
					pass
					#print "t",
				allres.append(res)
		
		return allres
	
	def gen_histgram(self):
		oline=[]
		for p in range(cat_num):
			poori_t=[0.0 for x in range(10)]
			poori_f=[0.0 for x in range(10)]
			for q in range(rpc):
				
				for s in self.r[p*rpc+q]:
					if s[0]==float("inf"):
						continue
					if s[1]==True:
						if s[0]>=5.0:
							poori_t[-1]+=1
						else:
							poori_t[int(s[0]/0.5)]+=1
					
					elif s[1]==False:
						if s[0]>=5.0:
							poori_f[-1]+=1
						else:
							poori_f[int(s[0]/0.5)]+=1
			
			tmp_oline=""
			p=0
			for q,u in zip(poori_t[:-1],poori_f[:-1]):
				tmp_oline+=str(p*0.5)+"~"+str(p*0.5+0.5)+","+str(q)+","+str(u)+"\n"
				p+=1
			
			tmp_oline+="4.5~,"+str(poori_t[-1])+","+str(poori_f[-1])+"\n"
			oline.append(tmp_oline)
		
		return oline
	
	def gen_histgram_bycvc(self,a,b):
		oline=""
		for p in self.r[a*rpc:(a+1)*rpc]:
			
			pass
	
	def inf_check(self):
		part_inf=0;all_inf=0;
		for p in range(cat_num*rpc):
			inf_num=0;
			
			for q in self.r[p]:
				if q[0]==float("inf"):
					inf_num+=1
			
			if inf_num==1999:
				all_inf+=1
			elif inf_num==703:
				part_inf+=1
			else:
				pass
		
		print "all inf",all_inf
		print "part inf",part_inf

file_select=4#説明むずい

class dic_to_file:
	
	def __init__(self,recipe_dir):
		if file_select==4:
			self.filelist=["Fried_Chicken500.tsv","Hamburger_Steak500.tsv",
			"Miso_Soup500.tsv","Potage500.tsv"]
		elif file_select==17:
			self.filelist=["Gyu-don.txt","Natto_Fried_rice.txt","Sushi_Role.txt","Chirashi_Sushi.txt",
			"Fried_chicken.txt","Omelet.txt","Tofu_humburg_steak.txt","Cream_stew.txt",
			"Subuta.txt","Oyako-don.txt","Beef_steak.txt","Kaisen-don.txt",
			"Pork_ginger.txt","Chicken_carry.txt","Salad.txt","Cream_puff.txt","Smoothie.txt"]
		else:
			pass
		
		self.recipe_dir=recipe_dir
	
	def call_text(self,num):
		fp=open(self.recipe_dir+self.filelist[int(num/rpc)])
		A=fp.readlines()
		return A[num%rpc].strip()
